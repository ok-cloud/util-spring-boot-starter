<p align="center">
    <p align="center">
        一个常用的 SpringBoot 项目工具类 SDK！
    </p>
    <p align="center">
        <img src="https://img.shields.io/badge/jdk-1.8-brightgreen">
        <img src="https://img.shields.io/badge/maven-3.6.1-brightgreen">
        <img src="https://img.shields.io/badge/springboot-2.7.3-brightgreen">
        <img src="https://img.shields.io/badge/license-MulanPSL-yellowgreen">
    </p>
</p>

---

## 项目简介

项目起源的初衷是由于作者在个人项目和公司项目中会用到很多常用的工具类，然而每个项目都写了很多相同工具的重复代码，为避免重复操作，故而将这些常用的工具类抽取出公共 SDK 来供其他项目使用，也好达到统一升级的好处。

## 快速开始

```xml
<dependency>
    <groupId>com.gitee.ok-tool</groupId>
    <artifactId>util-spring-boot-starter</artifactId>
    <version>master-SNAPSHOT</version>
</dependency>
```

注意：引入以上依赖需要修改两个地方。

1、SpringBoot pom.xml

```xml
<repositories>
    <repository>
        <id>jitpack.io</id>
        <url>https://jitpack.io</url>
    </repository>
</repositories>
```

2、Maven settings.xml

```xml
<mirror>
    <id>nexus-aliyun</id>
    <mirrorOf>*,!jitpack.io</mirrorOf>
    <name>Nexus aliyun</name>
    <url>http://maven.aliyun.com/nexus/content/groups/public</url>
</mirror>
```

参考资料：https://jitpack.io/#com.gitee.ok-tool/util-spring-boot-starter

## 本地使用

1、克隆项目

```
git clone https://gitee.com/ok-tool/util-spring-boot-starter.git
```

2、执行打包命令

```
mvn install -Dmaven.test.skip=true
```

3、引入依赖

```xml
<dependency>
    <groupId>cn.xlbweb</groupId>
    <artifactId>util-spring-boot-starter</artifactId>
    <version>1.0.0</version>
</dependency>
```

4、如何使用

- Wiki 文档：https://gitee.com/ok-tool/util-spring-boot-starter/wikis/pages
- 接入项目参考：https://gitee.com/wudibo/ok-simple-cli

## 未来规划

- 依赖上传到 Maven 中央仓库，替换掉 JitPack；
- 在依赖外部最小的情况下，引入更多场景；

如果有任何想法或意见，欢迎 issue 或 pr，QQ 交流群：956194623