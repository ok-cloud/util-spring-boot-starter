package cn.xlbweb.plus.common;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * 自定义配置
 *
 * @author: wudibo
 * @since: 1.0.0
 */
@Configuration
public class BizProperties {

    private static final Log logger = LogFactory.getLog(BizProperties.class);

    /**
     * 成功响应码
     */
    @Value("${cn.xlbweb.plus.response-success-code:0}")
    private Integer responseSuccessCode;

    /**
     * 失败响应码
     */
    @Value("${cn.xlbweb.plus.response-error-code:-1}")
    private Integer responseErrorCode;

    /**
     * 是否允许跨域
     */
    @Value("${cn.xlbweb.plus.cors-enabled:false}")
    private Boolean corsEnabled;

    /**
     * RestTemplate连接超时时间
     */
    @Value("${cn.xlbweb.plus.rt-connect-timeout:3000}")
    private Integer rtConnectTimeout;

    /**
     * RestTemplate数据读取超时时间
     */
    @Value("${cn.xlbweb.plus.rt-read-timeout:3000}")
    private Integer rtReadTimeout;

    /**
     * RestTemplate从连接池获取请求连接的超时时间
     */
    @Value("${cn.xlbweb.plus.rt-connection-request-timeout:10000}")
    private Integer rtConnectionRequestTimeout;

    /**
     * RestTemplate连接池最大连接数
     */
    @Value("${cn.xlbweb.plus.rt-pool-max-total:300}")
    private Integer rtPoolMaxTotal;

    /**
     * RestTemplate每个主机连接的最大并发数
     */
    @Value("${cn.xlbweb.plus.rt-pool-default-max-per-route:200}")
    private Integer rtPoolDefaultMaxPerRoute;

    /**
     * RestTemplate可用空闲连接过期时间
     */
    @Value("${cn.xlbweb.plus.rt-pool-validate-after-inactivity:30000}")
    private Integer rtPoolValidateAfterInactivity;

    public Integer getResponseSuccessCode() {
        return responseSuccessCode;
    }

    public void setResponseSuccessCode(Integer responseSuccessCode) {
        this.responseSuccessCode = responseSuccessCode;
    }

    public Integer getResponseErrorCode() {
        return responseErrorCode;
    }

    public void setResponseErrorCode(Integer responseErrorCode) {
        this.responseErrorCode = responseErrorCode;
    }

    public Boolean getCorsEnabled() {
        return corsEnabled;
    }

    public void setCorsEnabled(Boolean corsEnabled) {
        this.corsEnabled = corsEnabled;
    }

    public Integer getRtConnectTimeout() {
        return rtConnectTimeout;
    }

    public void setRtConnectTimeout(Integer rtConnectTimeout) {
        this.rtConnectTimeout = rtConnectTimeout;
    }

    public Integer getRtReadTimeout() {
        return rtReadTimeout;
    }

    public void setRtReadTimeout(Integer rtReadTimeout) {
        this.rtReadTimeout = rtReadTimeout;
    }

    public Integer getRtConnectionRequestTimeout() {
        return rtConnectionRequestTimeout;
    }

    public void setRtConnectionRequestTimeout(Integer rtConnectionRequestTimeout) {
        this.rtConnectionRequestTimeout = rtConnectionRequestTimeout;
    }

    public Integer getRtPoolMaxTotal() {
        return rtPoolMaxTotal;
    }

    public void setRtPoolMaxTotal(Integer rtPoolMaxTotal) {
        this.rtPoolMaxTotal = rtPoolMaxTotal;
    }

    public Integer getRtPoolDefaultMaxPerRoute() {
        return rtPoolDefaultMaxPerRoute;
    }

    public void setRtPoolDefaultMaxPerRoute(Integer rtPoolDefaultMaxPerRoute) {
        this.rtPoolDefaultMaxPerRoute = rtPoolDefaultMaxPerRoute;
    }

    public Integer getRtPoolValidateAfterInactivity() {
        return rtPoolValidateAfterInactivity;
    }

    public void setRtPoolValidateAfterInactivity(Integer rtPoolValidateAfterInactivity) {
        this.rtPoolValidateAfterInactivity = rtPoolValidateAfterInactivity;
    }
}
