package cn.xlbweb.plus.spring;

import cn.xlbweb.plus.common.BizProperties;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 常用web mvc配置
 *
 * @author: wudibo
 * @since: 1.0.0
 */
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

    private static final Log logger = LogFactory.getLog(WebMvcConfig.class);

    @Autowired
    private BizProperties bizProperties;

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        if (bizProperties.getCorsEnabled()) {
            registry.addMapping("/**").allowedOrigins("*").allowCredentials(true).allowedMethods("*").allowedHeaders("*").maxAge(3600);
        }
    }
}
