package cn.xlbweb.plus.spring;

import cn.xlbweb.plus.util.JsonUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Servlet工具类
 *
 * @author: wudibo
 * @since: 1.0.0
 */
public class ServletUtils {

    private static final Log logger = LogFactory.getLog(ServletUtils.class);

    /**
     * 返回json数据
     *
     * @param response response对象
     * @param data     目标
     */
    public static void outputJson(HttpServletResponse response, Object data) {
        PrintWriter writer = null;
        response.setCharacterEncoding("utf-8");
        response.setContentType("application/json; charset=utf-8");
        try {
            writer = response.getWriter();
            writer.print(JsonUtils.toJsonString(data));
        } catch (IOException e) {
            logger.error("print response error :", e);
        } finally {
            if (writer != null) {
                writer.close();
            }
        }
    }

    /**
     * 获取当前现场的request对象
     *
     * @return request 对象
     */
    public static HttpServletRequest getCurrentRequest() {
        return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
    }
}
