package cn.xlbweb.plus.util;

import cn.xlbweb.plus.exception.BizException;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.util.CollectionUtils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

/**
 * 枚举工具类
 *
 * @author: wudibo
 * @since: 1.0.0
 */
public class EnumUtils {

    private static final Log logger = LogFactory.getLog(EnumUtils.class);

    public static final String NAME = "name";
    public static final String KEY = "key";
    public static final String VALUE = "value";
    public static final String GET_KEY = "getKey";
    public static final String GET_VALUE = "getValue";

    public static boolean existsEnum(Class<? extends Enum<?>> clazz, Object value, String type) {
        if (StringUtils.isBlank(type)) {
            throw new BizException("type must be not null");
        }

        // 比较 name
        if (StringUtils.equals(type, NAME)) {
            List<String> names = getNames(clazz);
            if (CollectionUtils.isEmpty(names)) {
                return false;
            }

            if (!names.contains(value)) {
                return false;
            }
            return true;
        }

        // 比较 key 或 value
        return compareValue(clazz, value, type);
    }

    private static boolean compareValue(Class<? extends Enum<?>> clazz, Object value, String type) {
        String method = StringUtils.equalsIgnoreCase(type, KEY) ? GET_KEY : GET_VALUE;
        Enum<?>[] enumConstants = clazz.getEnumConstants();
        for (Enum<?> enumConstant : enumConstants) {
            String sourceValue = String.valueOf(value);
            String targetValue = String.valueOf(invokeEnumMethod(clazz, method, enumConstant));
            if (StringUtils.equals(sourceValue, targetValue)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 获取所有的 name 集合
     *
     * @param clazz 枚举类
     * @return 所有的 name 集合
     */
    public static List<String> getNames(Class<? extends Enum<?>> clazz) {
        final Enum<?>[] enums = clazz.getEnumConstants();
        if (Objects.isNull(enums)) {
            return null;
        }

        final List<String> list = new ArrayList<>(enums.length);
        for (Enum<?> anEnum : enums) {
            list.add(anEnum.name());
        }
        return list;
    }

    /**
     * 将单个枚举类转换为 json 对象
     *
     * @param enumClass 单个枚举类
     * @return json 对象
     */
    public static List<Map<String, Object>> getJson(Class<? extends Enum<?>> enumClass) {
        List<Map<String, Object>> list = new ArrayList<>();
        Enum<?>[] enumConstants = enumClass.getEnumConstants();
        for (Enum<?> enumConstant : enumConstants) {
            Map<String, Object> item = new HashMap<>(4);
            item.put(KEY, invokeEnumMethod(enumClass, GET_KEY, enumConstant));
            item.put(VALUE, invokeEnumMethod(enumClass, GET_VALUE, enumConstant));
            list.add(item);
        }
        return list;
    }

    /**
     * 将多个枚举类转换为 json 对象
     *
     * @param enumClasses 多个枚举类
     * @return json 对象集合
     */
    public static Map<String, List<Map<String, Object>>> getJson(Class<? extends Enum<?>>... enumClasses) {
        // 默认传入最多24个枚举类
        Map<String, List<Map<String, Object>>> list = new HashMap<>(32);
        for (Class<? extends Enum<?>> enumClass : enumClasses) {
            list.put(dealEnumName(enumClass.getSimpleName()), getJson(enumClass));
        }
        return list;
    }

    /**
     * 将某个 package 目录下的所有枚举类转换为 json 对象，默认不递归子文件夹
     *
     * @param packageName package 名称
     * @return json 对象集合
     */
    public static Map<String, List<Map<String, Object>>> getJson(String packageName) {
        return getJson(packageName, Boolean.FALSE);
    }

    /**
     * 将某个 package 目录下的所有枚举类转换为 json 对象
     *
     * @param packageName package 名称
     * @param recursive   是否递归子文件夹
     * @return json 对象集合
     */
    public static Map<String, List<Map<String, Object>>> getJson(String packageName, boolean recursive) {
        List<Class<? extends Enum<?>>> enumClasses = ClassUtils.getEnumClasses(packageName, recursive);
        // 默认传入最多24个枚举类
        Map<String, List<Map<String, Object>>> list = new HashMap<>(32);
        for (Class<? extends Enum<?>> enumClass : enumClasses) {
            list.put(dealEnumName(enumClass.getSimpleName()), getJson(enumClass));
        }
        return list;
    }

    private static Object invokeEnumMethod(Class clazz, String invokeMethod, Enum<?> invokeClass) {
        Method method;
        try {
            method = clazz.getMethod(invokeMethod);
            return method.invoke(invokeClass);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            logger.error(e);
        }
        return null;
    }

    private static String dealEnumName(String enumName) {
        enumName = enumName.replace("Enum", "");
        char[] charArray = enumName.toCharArray();
        charArray[0] += 32;
        return String.valueOf(charArray);
    }

    public static Object keyToValue() {
        return null;
    }

    public static Object valueToKey() {
        return null;
    }
}
