package cn.xlbweb.plus.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.UnknownHostException;

/**
 * 地址工具类
 *
 * @author: wudibo
 * @since: 1.0.0
 */
public class AddrUtils {

    private static final Log logger = LogFactory.getLog(AddrUtils.class);

    /**
     * 获取本地IP地址
     *
     * @return 本地IP地址
     */
    public static String getLocalIp() {
        try {
            return InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            logger.error("get local ip error", e);
        }
        return null;
    }

    /**
     * 获取客户端IP地址
     *
     * @return 客户端IP地址
     */
    public static String getRemoteIp() {
        return null;
    }

    /**
     * 获取本地Mac地址
     *
     * @return 本地mac地址
     */
    public static String getLocalMac() {
        try {
            InetAddress inetAddress = InetAddress.getLocalHost();
            byte[] hardwareAddress = NetworkInterface.getByInetAddress(inetAddress).getHardwareAddress();
            StringBuilder mac = new StringBuilder();
            for (int i = 0; i < hardwareAddress.length; i++) {
                if (i != 0) {
                    mac.append("-");
                }
                int temp = hardwareAddress[i] & 0xff;
                String str = Integer.toHexString(temp);
                if (str.length() == 1) {
                    mac.append("0" + str);
                } else {
                    mac.append(str);
                }
            }
            return mac.toString().toUpperCase();
        } catch (Exception e) {
            logger.error("get local mac error", e);
        }
        return null;
    }
}
