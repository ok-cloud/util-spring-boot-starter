package cn.xlbweb.plus.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.List;

/**
 * Json工具类
 *
 * @author: wudibo
 * @since: 1.0.0
 */
public class JsonUtils {

    private static final Log logger = LogFactory.getLog(JsonUtils.class);

    private static final ObjectMapper objectMapper = new ObjectMapper();

    /**
     * 对象 - json字符串
     *
     * @param obj 对象
     * @return json字符串
     */
    public static String toJsonString(Object obj) {
        try {
            return objectMapper.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            logger.error("object to json parse error", e);
        }
        return null;
    }

    /**
     * 对象 - json字符串并格式化
     *
     * @param obj 对象
     * @return json字符串并格式化
     */
    public static String toJsonStringFormat(Object obj) {
        try {
            return objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            logger.error("object to json parse error", e);
        }
        return null;
    }

    /**
     * json字符串 - 指定类型的对象
     *
     * @param json  json字符串
     * @param clazz 指定类型的对象
     * @param <T>   T
     * @return 指定类型的对象
     */
    public static <T> T toObj(String json, Class<T> clazz) {
        try {
            return objectMapper.readValue(json, clazz);
        } catch (JsonProcessingException e) {
            logger.error("json to object parse error", e);
        }
        return null;
    }

    /**
     * 对象 - 指定类型的对象
     *
     * @param obj   对象
     * @param clazz 指定类型的对象
     * @param <T>   T
     * @return 指定类型的对象
     */
    public static <T> T toObj(Object obj, Class<T> clazz) {
        try {
            return objectMapper.readValue(toJsonString(obj), clazz);
        } catch (JsonProcessingException e) {
            logger.error("json to object parse error", e);
        }
        return null;
    }

    /**
     * json字符串 - 指定类型的对象集合
     *
     * @param json  json字符串
     * @param clazz 指定类型的对象集合
     * @param <T>   T
     * @return 指定类型的对象集合
     */
    public static <T> List<T> toObjs(String json, Class<T> clazz) {
        try {
            return objectMapper.readValue(json, objectMapper.getTypeFactory().constructCollectionType(List.class, clazz));
        } catch (JsonProcessingException e) {
            logger.error("json to objects parse error", e);
        }
        return null;
    }

    /**
     * 从json串中获取某个key的value
     * eg: data/username
     *
     * @param obj     对象
     * @param keyPath 路径
     * @param clazz   转换类型
     * @param <T>     T
     * @return value
     */
    public static <T> T getJsonVal(Object obj, String keyPath, Class<T> clazz) {
        try {
            JsonNode jsonNode = objectMapper.readTree(toJsonString(obj));
            String[] keys = StringUtils.split(keyPath, "/");
            for (String key : keys) {
                jsonNode = getNextJsonNode(jsonNode, key);
            }
            logger.info("jsonNode type is " + jsonNode.getNodeType());
            return toObj(jsonNode, clazz);
        } catch (JsonProcessingException e) {
            logger.error("get json val error", e);
        }
        return null;
    }

    private static JsonNode getNextJsonNode(JsonNode jsonNode, String key) {
        return jsonNode.get(key);
    }
}
