package cn.xlbweb.plus.util;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * ID工具类
 *
 * @author: wudibo
 * @since: 1.0.0
 */
public class IdUtils {

    private static final Log logger = LogFactory.getLog(IdUtils.class);

    public static List<String> toListString(String ids) {
        Validate.notNull(ids, "The ids must not be null");
        String[] idArr = StringUtils.split(ids, ",");
        return Stream.of(idArr).collect(Collectors.toList());
    }

    public static List<Integer> toListInteger(String ids) {
        Validate.notNull(ids, "The ids must not be null");
        String[] idArr = StringUtils.split(ids, ",");
        return Arrays.stream(idArr).map(id -> Integer.valueOf(id)).collect(Collectors.toList());
    }

    public static <T> List<T> toListBeanString(String ids, Supplier<T> target) {
        Validate.notNull(ids, "The ids must not be null");
        List<String> idList = toListString(ids);
        List<T> list = new ArrayList<>(idList.size());
        for (String id : idList) {
            list.add(generatorBean(target, id));
        }
        return list;
    }

    public static <T> List<T> toListBeanInteger(String ids, Supplier<T> target) {
        Validate.notNull(ids, "The ids must not be null");
        List<Integer> idList = toListInteger(ids);
        List<T> list = new ArrayList<>(idList.size());
        for (Integer id : idList) {
            list.add(generatorBean(target, id));
        }
        return list;
    }

    private static <T> T generatorBean(Supplier<T> target, Object id) {
        try {
            T t = target.get();
            Method[] methods = t.getClass().getMethods();
            for (Method method : methods) {
                if (StringUtils.equals("setId", method.getName())) {
                    method.invoke(t, id);
                }
            }
        } catch (IllegalAccessException | InvocationTargetException e) {
            logger.error(e);
        }
        return null;
    }

    public static String getUuid() {
        return UUID.randomUUID().toString().replace("-", "");
    }

    public static String getSnowFlakeId() {
        return "";
    }
}
