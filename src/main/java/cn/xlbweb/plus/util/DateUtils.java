package cn.xlbweb.plus.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAdjusters;
import java.util.Date;

/**
 * 日期工具类
 *
 * @author: wudibo
 * @since: 1.0.0
 */
public class DateUtils {

    private static final Log logger = LogFactory.getLog(DateUtils.class);

    public final static String YYYY_MM_DD = "yyyy-MM-dd";
    public final static String YYYY_MM_DD_HH_MM = "yyyy-MM-dd HH:mm";
    public final static String YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";
    public final static String YYYY_MM = "yyyy-MM";

    /**
     * 获取当前时间的格式化字符串
     *
     * @param format 格式化类型
     * @return 格式化后的字符串日期
     */
    public static String getNowFormat(String format) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(format);
        return LocalDateTime.now().format(dateTimeFormatter);
    }

    /**
     * 为当前时间加上多少分钟
     *
     * @param minutes 分钟
     * @return Date
     */
    public static Date plusMinutes(long minutes) {
        LocalDateTime localDateTime = LocalDateTime.now().plusMinutes(minutes);
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }

    /**
     * 获取本月的第一天
     *
     * @return 字符串日期
     */
    public static String getFirstDayOfMonth() {
        return LocalDate.now().with(TemporalAdjusters.firstDayOfMonth()) + " 00:00:00";
    }

    /**
     * 获取本月的最后一天
     *
     * @return 字符串日期
     */
    public static String getLastDayOfMonth() {
        return LocalDate.now().with(TemporalAdjusters.lastDayOfMonth()) + " 23:59:59";
    }
}
