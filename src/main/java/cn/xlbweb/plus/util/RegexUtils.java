package cn.xlbweb.plus.util;

import java.util.regex.Pattern;

/**
 * 常用正则表达式
 *
 * @author: wudibo
 * @since: 1.0.0
 */
public class RegexUtils {

    /**
     * 手机号码
     */
    public final static String PHONE = "^1[0-9]{10}$";

    /**
     * 邮箱
     */
    public final static String MAIL = "^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)+$";

    /**
     * 身份证
     */
    public final static String ID_CARD = "(^\\d{15}$)|(^\\d{18}$)|(^\\d{17}(\\d|X|x)$)";

    /**
     * 数字
     */
    public final static String NUMBER = "^[0-9]*$";

    /**
     * 汉字
     */
    public final static String CHINESE_CHARACTER = "^[\\u4e00-\\u9fa5]{0,}$";

    /**
     * 小写字母
     */
    public final static String LETTER_LOWERCASE = "^[a-z]+$";

    /**
     * 大写字母
     */
    public final static String LETTER_UPPERCASE = "^[A-Z]+$";

    /**
     * 不区分大小写字母
     */
    public final static String LETTER_IGNORECASE = "^[A-Za-z]+$";

    /**
     * 域名
     */
    public final static String DOMAIN = "[a-zA-Z0-9][-a-zA-Z0-9]{0,62}(\\.[a-zA-Z0-9][-a-zA-Z0-9]{0,62})+\\.?";

    /**
     * IP V4
     */
    public final static String IP_V4 = "[a-zA-Z0-9][-a-zA-Z0-9]{0,62}(\\.[a-zA-Z0-9][-a-zA-Z0-9]{0,62})+\\.?";

    /**
     * IP V6
     */
    public final static String IP_V6 = "^((([0-9A-Fa-f]{1,4}:){7}[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){1,7}:)|(([0-9A-Fa-f]{1,4}:){6}:[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){5}(:[0-9A-Fa-f]{1,4}){1,2})|(([0-9A-Fa-f]{1,4}:){4}(:[0-9A-Fa-f]{1,4}){1,3})|(([0-9A-Fa-f]{1,4}:){3}(:[0-9A-Fa-f]{1,4}){1,4})|(([0-9A-Fa-f]{1,4}:){2}(:[0-9A-Fa-f]{1,4}){1,5})|([0-9A-Fa-f]{1,4}:(:[0-9A-Fa-f]{1,4}){1,6})|(:(:[0-9A-Fa-f]{1,4}){1,7})|(([0-9A-Fa-f]{1,4}:){6}(\\\\d|[1-9]\\\\d|1\\\\d{2}|2[0-4]\\\\d|25[0-5])(\\\\.(\\\\d|[1-9]\\\\d|1\\\\d{2}|2[0-4]\\\\d|25[0-5])){3})|(([0-9A-Fa-f]{1,4}:){5}:(\\\\d|[1-9]\\\\d|1\\\\d{2}|2[0-4]\\\\d|25[0-5])(\\\\.(\\\\d|[1-9]\\\\d|1\\\\d{2}|2[0-4]\\\\d|25[0-5])){3})|(([0-9A-Fa-f]{1,4}:){4}(:[0-9A-Fa-f]{1,4}){0,1}:(\\\\d|[1-9]\\\\d|1\\\\d{2}|2[0-4]\\\\d|25[0-5])(\\\\.(\\\\d|[1-9]\\\\d|1\\\\d{2}|2[0-4]\\\\d|25[0-5])){3})|(([0-9A-Fa-f]{1,4}:){3}(:[0-9A-Fa-f]{1,4}){0,2}:(\\\\d|[1-9]\\\\d|1\\\\d{2}|2[0-4]\\\\d|25[0-5])(\\\\.(\\\\d|[1-9]\\\\d|1\\\\d{2}|2[0-4]\\\\d|25[0-5])){3})|(([0-9A-Fa-f]{1,4}:){2}(:[0-9A-Fa-f]{1,4}){0,3}:(\\\\d|[1-9]\\\\d|1\\\\d{2}|2[0-4]\\\\d|25[0-5])(\\\\.(\\\\d|[1-9]\\\\d|1\\\\d{2}|2[0-4]\\\\d|25[0-5])){3})|([0-9A-Fa-f]{1,4}:(:[0-9A-Fa-f]{1,4}){0,4}:(\\\\d|[1-9]\\\\d|1\\\\d{2}|2[0-4]\\\\d|25[0-5])(\\\\.(\\\\d|[1-9]\\\\d|1\\\\d{2}|2[0-4]\\\\d|25[0-5])){3})|(:(:[0-9A-Fa-f]{1,4}){0,5}:(\\\\d|[1-9]\\\\d|1\\\\d{2}|2[0-4]\\\\d|25[0-5])(\\\\.(\\\\d|[1-9]\\\\d|1\\\\d{2}|2[0-4]\\\\d|25[0-5])){3}))$";

    /**
     * URL
     */
    public final static String URL = "[a-zA-z]+://[^\\s]*";

    /**
     * QQ号码
     */
    public final static String QQ = "[1-9][0-9]{4,}";

    /**
     * 邮政编码
     */
    public final static String POST_CODE = "[1-9]\\d{5}(?!\\d)";

    /**
     * 强密码（必须包含大小写字母和数字的组合，不能使用特殊字符，长度在 8-20 之间）
     */
    public final static String STRENGTH_PASSWORD1 = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])[a-zA-Z0-9]{8,20}$";

    /**
     * 强密码（必须包含大小写字母和数字的组合，可以使用特殊字符，长度在 8-20 之间）
     */
    public final static String STRENGTH_PASSWORD2 = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,20}$";

    /**
     * 年月日：yyyy-MM-dd
     */
    public final static String YYYY_MM_DD = "^(([0-9]{3}[1-9]|[0-9]{2}[1-9][0-9]{1}|[0-9]{1}[1-9][0-9]{2}|[1-9][0-9]{3})-(((0[13578]|1[02])-(0[1-9]|[12][0-9]|3[01]))|((0[469]|11)-(0[1-9]|[12][0-9]|30))|(02-(0[1-9]|[1][0-9]|2[0-8]))))|((([0-9]{2})(0[48]|[2468][048]|[13579][26])|((0[48]|[2468][048]|[3579][26])00))-02-29)$";

    /**
     * 年月日时分：yyyy-MM-dd HH:mm
     */
    public final static String YYYY_MM_DD_HH_MM = "^[1-2][0-9][0-9][0-9]-([1][0-2]|0?[1-9])-([12][0-9]|3[01]|0?[1-9]) ([01][0-9]|[2][0-3]):[0-5][0-9]$";

    /**
     * 年月日时分秒：yyyy-MM-dd HH:mm:ss
     */
    public final static String YYYY_MM_DD_HH_MM_SS = "^[1-2][0-9][0-9][0-9]-([1][0-2]|0?[1-9])-([12][0-9]|3[01]|0?[1-9]) ([01][0-9]|[2][0-3]):[0-5][0-9]:[0-5][0-9]$";

    /**
     * 字符串正则校验
     *
     * @param str   字符串
     * @param regex 正则表达式
     * @return 校验结果(true / false)
     */
    public static boolean regexCheck(String str, String regex) {
        return Pattern.matches(regex, str);
    }
}
