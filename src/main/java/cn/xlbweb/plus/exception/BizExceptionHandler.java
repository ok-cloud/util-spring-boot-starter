package cn.xlbweb.plus.exception;

import cn.xlbweb.plus.response.ServerResponse;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.validation.BindException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.Objects;

/**
 * 常用异常拦截处理
 *
 * @author: wudibo
 * @since: 1.0.0
 */
@RestControllerAdvice
@ConditionalOnProperty(prefix = "cn.xlbweb.util", name = "exception-handler-enabled", havingValue = "true")
public class BizExceptionHandler {

    private static final Log logger = LogFactory.getLog(BizExceptionHandler.class);

    @ExceptionHandler(value = BindException.class)
    public ServerResponse bindException(BindException e) {
        logger.error("参数校验异常1", e);
        String message = e.getFieldError().getDefaultMessage();
        return ServerResponse.error(message);
    }

    @ExceptionHandler(value = ConstraintViolationException.class)
    public ServerResponse constraintViolationException(ConstraintViolationException e) {
        logger.error("参数校验异常2", e);
        String message = new ArrayList<>(e.getConstraintViolations()).get(0).getMessage();
        return ServerResponse.error(message);
    }

    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public ServerResponse methodArgumentNotValidException(MethodArgumentNotValidException e) {
        logger.error("参数校验异常3", e);
        String message = e.getBindingResult().getFieldError().getDefaultMessage();
        return ServerResponse.error(message);
    }

    @ExceptionHandler(value = IllegalArgumentException.class)
    public ServerResponse illegalArgumentException(IllegalArgumentException e) {
        logger.error("参数校验异常4", e);
        return ServerResponse.error(e.getMessage());
    }

    @ExceptionHandler(value = BizException.class)
    public ServerResponse bizException(BizException e) {
        logger.error("自定义异常", e);
        if (Objects.nonNull(e.getCode())) {
            return ServerResponse.error(e.getCode(), e.toString());
        }
        return ServerResponse.error(e.toString());
    }

    @ExceptionHandler(value = Exception.class)
    public ServerResponse exception(Exception e) {
        logger.error("发生异常", e);
        if (StringUtils.isNotBlank(e.getMessage())) {
            return ServerResponse.error(e.getMessage());
        }
        return ServerResponse.error(e.toString());
    }
}
