package cn.xlbweb.plus.exception;

/**
 * 自定义业务异常
 *
 * @author: wudibo
 * @since: 1.0.0
 */
public class BizException extends RuntimeException {

    private Integer code;

    private String message;

    public BizException(String message) {
        this.message = message;
    }

    public BizException(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public BizException(String message, Throwable throwable) {
        super(message, throwable);
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
