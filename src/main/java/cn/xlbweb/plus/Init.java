package cn.xlbweb.plus;

import cn.xlbweb.plus.common.BizProperties;
import cn.xlbweb.plus.exception.BizExceptionHandler;
import cn.xlbweb.plus.http.HttpConfig;
import cn.xlbweb.plus.http.HttpUtils;
import cn.xlbweb.plus.spring.WebMvcConfig;
import cn.xlbweb.plus.spring.SpringUtils;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * Spring Bean初始化加载
 *
 * @author: wudibo
 * @since: 1.0.0
 */
@Configuration
@Import({BizProperties.class,
        SpringUtils.class,
        BizExceptionHandler.class,
        WebMvcConfig.class,
        HttpConfig.class,
        HttpUtils.class})
public class Init {
}
