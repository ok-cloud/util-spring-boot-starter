package cn.xlbweb.plus.response;

import cn.xlbweb.plus.spring.SpringUtils;
import cn.xlbweb.plus.common.BizProperties;

import java.io.Serializable;

/**
 * 数据表格响应
 *
 * @author: wudibo
 * @since: 1.0.0
 */
public class TableResponse<T> implements Serializable {

    private static BizProperties bizProperties = SpringUtils.getBean(BizProperties.class);

    private final static String SUCCESS_MESSAGE = "查询成功";

    private final static String ERROR_MESSAGE = "查询失败";

    private Integer code;
    private String message;
    private long total;
    private T data;

    public TableResponse(long total, T data) {
        this.code = bizProperties.getResponseSuccessCode();
        this.message = SUCCESS_MESSAGE;
        this.total = total;
        this.data = data;
    }

    private TableResponse(String message, long total, T data) {
        this.code = bizProperties.getResponseSuccessCode();
        this.message = message;
        this.total = total;
        this.data = data;
    }

    public static <T> TableResponse<T> success(long total, T data) {
        return new TableResponse<>(total, data);
    }

    public static <T> TableResponse<T> success(String message, long total, T data) {
        return new TableResponse<>(message, total, data);
    }

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public long getTotal() {
        return total;
    }

    public T getData() {
        return data;
    }
}
