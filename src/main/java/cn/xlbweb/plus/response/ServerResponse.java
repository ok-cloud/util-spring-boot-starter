package cn.xlbweb.plus.response;

import cn.xlbweb.plus.spring.SpringUtils;
import cn.xlbweb.plus.common.BizProperties;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.slf4j.MDC;

import java.io.Serializable;

/**
 * 服务端响应
 *
 * @author: wudibo
 * @since: 1.0.0
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ServerResponse<T> implements Serializable {

    private static BizProperties bizProperties = SpringUtils.getBean(BizProperties.class);

    private final static String SUCCESS_MESSAGE = "操作成功";

    private final static String ERROR_MESSAGE = "操作失败";

    private String traceId = MDC.get("traceId");
    private int code;
    private String message;
    private T data;

    private ServerResponse(int code) {
        this.code = code;
    }

    private ServerResponse(int code, String message) {
        this.code = code;
        this.message = message;
    }

    private ServerResponse(int code, T data) {
        this.code = code;
        this.data = data;
    }

    private ServerResponse(int code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    @JsonIgnore
    public boolean isSuccess() {
        return this.code == bizProperties.getResponseSuccessCode();
    }

    public static <T> ServerResponse<T> success() {
        return new ServerResponse<>(bizProperties.getResponseSuccessCode(), SUCCESS_MESSAGE);
    }

    public static <T> ServerResponse<T> success(String msg) {
        return new ServerResponse<>(bizProperties.getResponseSuccessCode(), msg);
    }

    public static <T> ServerResponse<T> success(T data) {
        return new ServerResponse<>(bizProperties.getResponseSuccessCode(), SUCCESS_MESSAGE, data);
    }

    public static <T> ServerResponse<T> success(String msg, T data) {
        return new ServerResponse<>(bizProperties.getResponseSuccessCode(), msg, data);
    }

    public static <T> ServerResponse<T> error() {
        return new ServerResponse<>(bizProperties.getResponseErrorCode(), ERROR_MESSAGE);
    }

    public static <T> ServerResponse<T> error(String errorMessage) {
        return new ServerResponse<>(bizProperties.getResponseErrorCode(), errorMessage);
    }

    public static <T> ServerResponse<T> error(int errorCode, String errorMessage) {
        return new ServerResponse<>(errorCode, errorMessage);
    }

    public String getTraceId() {
        return traceId;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public T getData() {
        return data;
    }
}
