package cn.xlbweb.plus.valid;

import cn.xlbweb.plus.exception.BizException;
import org.hibernate.validator.HibernateValidator;
import org.springframework.util.CollectionUtils;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.Set;

/**
 * Hibernate Validation 校验工具类
 *
 * @author: wudibo
 * @since: 1.0.0
 */
public class ValidationUtils {

    private static Validator validator = Validation.byProvider(HibernateValidator.class).configure().failFast(Boolean.TRUE).buildValidatorFactory().getValidator();

    public static <T> void validate(T t, Class<?>... groups) {
        Set<ConstraintViolation<T>> constraintViolationSet = validator.validate(t, groups);
        if (!CollectionUtils.isEmpty(constraintViolationSet)) {
            throw new BizException(constraintViolationSet.iterator().next().getMessage());
        }
    }
}
