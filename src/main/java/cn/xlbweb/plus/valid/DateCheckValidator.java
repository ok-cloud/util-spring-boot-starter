package cn.xlbweb.plus.valid;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Date;
import java.util.Objects;
import java.util.regex.Pattern;

/**
 * 日期类型校验器
 *
 * @author: wudibo
 * @since: 1.0.0
 */
public class DateCheckValidator implements ConstraintValidator<DateCheck, Object> {

    private String[] pattern;
    private boolean nullable;

    @Override
    public void initialize(DateCheck dateCheck) {
        pattern = dateCheck.pattern();
        nullable = dateCheck.nullable();
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        if (Objects.isNull(value)) {
            return nullable;
        }

        if (value instanceof Date) {
            return true;
        }

        if (value instanceof String) {
            String strValue = String.valueOf(value);
            boolean isMatch = false;
            for (String p : pattern) {
                if (Pattern.matches(p, strValue)) {
                    isMatch = true;
                    break;
                }
            }
            return isMatch;
        }
        return false;
    }
}
