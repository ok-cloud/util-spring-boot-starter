package cn.xlbweb.plus.valid;

import cn.xlbweb.plus.util.EnumUtils;
import cn.xlbweb.plus.exception.BizException;
import org.apache.commons.lang3.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;
import java.util.Objects;

/**
 * 枚举类型校验校验器
 *
 * @author: wudibo
 * @since: 1.0.0
 */
public class EnumCheckValidator implements ConstraintValidator<EnumCheck, Object> {

    private Class<? extends Enum<?>> enumClass;
    private String type;
    private Boolean nullable;
    private Boolean multiple;
    private String separator;

    @Override
    public void initialize(EnumCheck enumCheck) {
        enumClass = enumCheck.enumClass();
        type = enumCheck.type();
        nullable = enumCheck.nullable();
        multiple = enumCheck.multiple();
        separator = enumCheck.separator();
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        if (Objects.isNull(value)) {
            return nullable;
        }

        validType(type);

        if (value instanceof String) {
            return compare(value);
        }

        if (value instanceof Integer) {
            return compare(value);
        }
        return false;
    }

    private void validType(String type) {
        String[] types = new String[]{"name", "key", "value"};
        if (!Arrays.asList(types).contains(type)) {
            throw new BizException("The value of the type field is incorrect, it should take the value: name or key or value");
        }
    }

    private boolean compare(Object value) {
        String str = String.valueOf(value);
        if (StringUtils.isBlank(str)) {
            return nullable;
        }

        if (this.multiple) {
            String[] arr = StringUtils.split(str, this.separator);
            boolean exists = true;
            for (String s : arr) {
                exists = EnumUtils.existsEnum(enumClass, s, type);
            }
            return exists;
        }
        return EnumUtils.existsEnum(enumClass, value, type);
    }
}
