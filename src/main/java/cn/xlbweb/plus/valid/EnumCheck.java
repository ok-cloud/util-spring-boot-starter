package cn.xlbweb.plus.valid;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 枚举类型校验
 *
 * @author: wudibo
 * @since: 1.0.0
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = EnumCheckValidator.class)
public @interface EnumCheck {

    String message() default "枚举类型校验失败";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    /**
     * 枚举类
     *
     * @return
     */
    Class<? extends Enum<?>> enumClass();

    /**
     * 比较类型
     *
     * @return
     */
    String type() default "key";

    /**
     * 是否允许为空
     *
     * @return
     */
    boolean nullable() default false;

    /**
     * 是否支持多选
     *
     * @return
     */
    boolean multiple() default false;

    /**
     * 多选分隔符
     *
     * @return
     */
    String separator() default ",";
}
