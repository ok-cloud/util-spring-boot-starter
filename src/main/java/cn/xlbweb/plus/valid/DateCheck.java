package cn.xlbweb.plus.valid;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 日期类型校验
 *
 * @author: wudibo
 * @since: 1.0.0
 */
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = DateCheckValidator.class)
public @interface DateCheck {

    String message() default "日期类型校验失败";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    /**
     * 日期格式
     *
     * @return
     */
    String[] pattern() default {"yyyy-MM-dd"};

    /**
     * 是否允许为空
     *
     * @return
     */
    boolean nullable() default false;
}
