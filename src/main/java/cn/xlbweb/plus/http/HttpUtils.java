package cn.xlbweb.plus.http;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;

/**
 * Http工具类
 *
 * @author: wudibo
 * @since: 1.0.0
 */
@Component
public class HttpUtils {

    private static final Log logger = LogFactory.getLog(HttpUtils.class);

    @Autowired
    private RestTemplate rt;

    private static RestTemplate restTemplate;

    @PostConstruct
    public void init() {
        restTemplate = rt;
    }

    // GET

    public static <T> String get(String url) {
        ResponseEntity<String> responseEntity = restTemplate.getForEntity(url, String.class);
        if (responseEntity.getStatusCode() != HttpStatus.OK) {
            logger.error("do get error");
            return null;
        }
        return responseEntity.getBody();
    }

    public static <T> T get(String url, Class<T> responseType) {
        ResponseEntity<T> responseEntity = restTemplate.getForEntity(url, responseType);
        return responseEntity.getBody();
    }

    public static <T> T get(String url, HttpHeaders httpHeaders, Class<T> responseType) {
        HttpEntity<T> httpEntity = new HttpEntity<>(httpHeaders);
        ResponseEntity<T> responseEntity = restTemplate.exchange(url, HttpMethod.GET, httpEntity, responseType);
        return responseEntity.getBody();
    }

    // POST

    public static <T> String post(String url, T body) {
        HttpEntity<T> httpEntity = new HttpEntity<>(body);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity(url, httpEntity, String.class);
        return responseEntity.getBody();
    }

    public static <T> String post(String url, T body, HttpHeaders httpHeaders) {
        HttpEntity<T> httpEntity = new HttpEntity<>(body, httpHeaders);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity(url, httpEntity, String.class);
        return responseEntity.getBody();
    }

    public static <T> T post(String url, T body, HttpHeaders httpHeaders, Class<T> responseType) {
        HttpEntity<T> httpEntity = new HttpEntity<>(body, httpHeaders);
        ResponseEntity<T> responseEntity = restTemplate.postForEntity(url, httpEntity, responseType);
        return responseEntity.getBody();
    }

    // PUT

    public static <T> String put(String url, T body) {
        HttpEntity<T> httpEntity = new HttpEntity<>(body);
        ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.PUT, httpEntity, String.class);
        return responseEntity.getBody();
    }

    public static <T> String put(String url, T body, HttpHeaders httpHeaders) {
        HttpEntity<T> httpEntity = new HttpEntity<>(body, httpHeaders);
        ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.PUT, httpEntity, String.class);
        return responseEntity.getBody();
    }

    public static <T> T put(String url, T body, HttpHeaders httpHeaders, Class<T> responseType) {
        HttpEntity<T> httpEntity = new HttpEntity<>(body, httpHeaders);
        ResponseEntity<T> responseEntity = restTemplate.exchange(url, HttpMethod.PUT, httpEntity, responseType);
        return responseEntity.getBody();
    }

    // DELETE

    public static <T> String delete(String url) {
        ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.DELETE, null, String.class);
        return responseEntity.getBody();
    }

    public static <T> String delete(String url, HttpHeaders httpHeaders) {
        HttpEntity<T> httpEntity = new HttpEntity<>(httpHeaders);
        ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.DELETE, httpEntity, String.class);
        return responseEntity.getBody();
    }

    public static <T> T delete(String url, HttpHeaders httpHeaders, Class<T> responseType) {
        HttpEntity<T> httpEntity = new HttpEntity<>(httpHeaders);
        ResponseEntity<T> responseEntity = restTemplate.exchange(url, HttpMethod.DELETE, httpEntity, responseType);
        return responseEntity.getBody();
    }
}
