package cn.xlbweb.plus.http;

import cn.xlbweb.plus.common.BizProperties;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

/**
 * RestTemplate自定义配置
 *
 * @author: wudibo
 * @since: 1.0.0
 */
@Configuration
public class HttpConfig {

    @Autowired
    private BizProperties bizProperties;

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder restTemplateBuilder) {
        RestTemplate restTemplate = restTemplateBuilder.build();
        HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
        // 连接超时时间,单位为毫秒,默认(),建议设置(3000)
        factory.setConnectTimeout(bizProperties.getRtConnectTimeout());
        // 数据读取超时时间,单位为毫秒,默认(),建议设置(3000)
        factory.setReadTimeout(bizProperties.getRtReadTimeout());
        // 从连接池获取请求连接的超时时间,单位为毫秒,默认(),建议设置(10000)
        factory.setConnectionRequestTimeout(bizProperties.getRtConnectionRequestTimeout());
        restTemplate.setRequestFactory(factory);
        // HttpClientBuilder(重要)
        factory.setHttpClient(httpClientBuilder().build());
        return restTemplate;
    }

    @Bean
    public HttpClientBuilder httpClientBuilder() {
        HttpClientBuilder httpClientBuilder = HttpClientBuilder.create();
        PoolingHttpClientConnectionManager pool = new PoolingHttpClientConnectionManager();
        // 连接池最大连接数,默认(),建议设置300
        pool.setMaxTotal(bizProperties.getRtPoolMaxTotal());
        // 每个主机连接的最大并发数,默认(),建议设置200
        pool.setDefaultMaxPerRoute(bizProperties.getRtPoolDefaultMaxPerRoute());
        // 可用空闲连接过期时间,单位为毫秒,默认(),建议设置30000
        pool.setValidateAfterInactivity(bizProperties.getRtPoolValidateAfterInactivity());
        httpClientBuilder.setConnectionManager(pool);
        return httpClientBuilder;
    }
}
